# Towers of Hanoi & Uninformed Search 
## CS4200_SP24_Assignment03_UninformedSearch
## Towers of Hanoi:
**What is the size of the state space?**
We can say the size of the state space in the towers of hanoi problem is 3^n, where n is the number of discs we have in the problem. The 3 represents the three different pegs we can stack the discs on, if we had more or less pegs we can adjust that number. Since every peg can hold any amount of discs (as long as the order is correct), we can call the size of the state space as 3^n (as opposed to calling it 3n). 

**What is the inital state?**
All the discs begin on one peg. Before the questions it is said that the state space is represented by three lists, one for each peg, and within that list would be whatever discs are currently on the peg. So the initial state would be three arrays with one being filled up with all the pegs ([1,...,n], [], [])

**What is the action space?**
Since there are certain constraints on what actions can be taken within the space, so the action space is clearly defined. Check if the peg contains any discs, if you choose a peg in which you want to move a disc from, check the other pegs to see where it can be moved. If a peg contains a disc that is smaller than the disc being moved, you cannot move there, and if it is bigger, then the disc can be moved to that peg. 

**What is the goal test?**
The goal test is just a check to see if all the pegs are on the leftmost peg. This test only checks that, and would not need to check if the discs are in the right order, as the constraints in the action space would not allow them to be in the wrong order. 

## Uninformed Search
**Depth-first tree search:**
Order states are expanded:
[S] -> [A] -> [C] -> [G]

The path returned is the same as it was expanded, as the goal would be along the inital path of the search

**Depth-first graph search:**
Order states are expanded:
[S] -> [A] -> [C] -> [D] -> [G] -> [B]

Path returned:
S -> A -> C -> D -> G 


**Breadth-first tree search:**
Order states are expanded:
[S] -> [A, B, D] -> [B, D, C] -> [D, C] -> [C, G] -> [G] -> []

Path returned:
S -> D -> G

**Breadth-first graph search:**
Order states are expanded:
[S] -> [A, B, D] -> [B, D, C] -> [D, C] -> [C, G] -> [G] -> []

Path returned by algorithm: 
S -> D -> G, it returns the path with the less amount of hops, but it is not the optimal path to take with the weighted edges.

**Uniform cost graph search:**
Order states are expanded:
[S] -> [A] -> [C] -> [G]
[S] -> [D] -> [G]
[S] -> [B] -> [D] -> [G]

Path returned by the algorithm:
S -> A -> C -> G, this is the least cost path to the goal.