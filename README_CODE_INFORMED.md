## Jacob R Lebold
Bachelor of Science in Computer Science

## Q/A
**How long did it take for me to complete this assignment?**

6+ hours

**What was the most challenging part for me?**

Creating the heuristic for the puzzle problem. After some research I was able to understand it, but at first it seemed a lot more complicated than it actually was. Once getting it I just outputted the results and saw how much more efficient the manhattan heuristic was compared to the hamming. 

**Compare uninformed and informed search**

Informed search is when you have a heuristic function to take advantage of during the search. This heuristic is essentially a "guess" on how far away each state is from the goal state, and it drives the decision making of your algorithm. Uninformed as no idea about the goal state, and the search algorithms do not have an informed guess about the distance from one state to the goal. That is the biggest difference between the two, the use of a heuristic to get from the start state to the goal state. 

**What is an admissible heuristic and why is it important?**

An admissible heuristic is a hueristic that always gives a cost that is lower or equal to an actual cost to the goal. It is important to have an admissible heuristic as they never overestimate the costs to the goal, leading to optimal solutions when finding a path to the goal state. When you have a heuristic that is not admissible, it is possible for a path to be found to the goal that is not the actual optimal path due to the overestimation of the heuristic. 


**Given a set of admissible heuristics, what is the definition of dominance and how dominance impacts informed search?**

When you have a set of admissible heuristics, then the heuristic that gives the highest estimate dominates the others and is the best heuristic to use for the search. As long as it is still admissible, it will be the best estimate to use in the search, and if within a set, that heuristic will be better to use than all the others, even though every heuristic is admissible. 


