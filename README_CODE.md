## Jacob Lebold
Bachelor of Science in Computer science

## Q/A
**How long did it take me to complete this assignment?**

8+ hours 

**What was the most challenging part of this assignment?**

The most challenging part was setting everything up, as it took a while for me to figure everything out. The real challenge though was Uniform Cost Search. I was confused on how to pass in the path_cost function in the node class into the UCS function itself, so instead of using a heuristic, I could use the path cost that is stored in each node. Eventually I was able to figure it out, and it was such a small change, all I had to do was set f = path_cost function, and the rest of the code stayed essentially the same. The relief I felt once it ran was incomparable to anything else!!!!

**What did I learn form the visualization**

To be honest, I could never get the visualization to work, so I just printed out the solution path. For some reason QPsolver was never able to be implemented and I'm not sure if that's the cause of it, but even in the demo the visualization functions you gave us never actually worked and would always run into errors. So instead I just printed the solution path that we could get from running the function without visualization and went with that. I believe having the visualizations would help me greatly, and its a shame I couldn't get them to work.

**Compare & Contrast graph and tree search**

Graph and Tree search are very similar, with the big difference being that in a tree search, you can run into infinite trees with repeating loops. In our assignment, I ran into that problem trying to implement Depth-first tree search, as when running the code it just got stuck in an infinite loop. Graph search allowed us to bypass infinite loops however becuase it would stop us from searching a node more than once. 

**In the code, what are the differences between implementing tree search and graph search?**

The biggest difference is using a set() to store nodes we have already searched, this stopped any infinite loops from occurring which allowed us to search every node. This was huge, because in Depth-first tree search we ran into an infinite loop, but in Depth-first graph search, using the set() we stopped that issue from occurring and allowed us to explore all the nodes. 

