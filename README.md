## Jacob Lebold
Bachelor of Science in Computer Science

## Q/A
**How long it took to complete this assignment:** 

3+ hours

**What is the most challenging part for myself:**

Working out the graph algorithms. Converting a graph to a tree was something that I was never confident in doing in my Algorithms class, and I saw the effect of not being well versed in how to do it here. This took the longest for me. The only time I had seen this was with a directed graph as well, so having a undirected graph made this an even greater challenge. 

**Compare BFS, DFS, and uniform cost search in terms of completeness, time complexity, space complexity, and optimality:**

BFS: Breadth First Search is a complete search algorithm, if there is an optimal answer, it will find it. However, since it works through every node, the runtime complexity is very slow (O(b^d+1)). Its space complexity is also very large since it keeps every node in memory (O(b^d+1)). In general, it is not optimal, the slow runtime and the large amount of space in memory it takes up means that for most searches, you would not want to use Breadth First Search

DFS: Depth First Search is not a complete search algorithm. If we have an infinite spanning tree, the entire tree will never be searched (as the search only goes down one path, unlike BFS). It has a runtime complexity of O(b^m), this is not good if m is much larger than d, but in dense solution spaces, it can be much faster than BFS. Space complexity is also much better than BFS, as it is O(bm) meaning it has a linear space complexity which is very good! Unfortunatley it is not optimal, as it is not complete, and has the the potential for a very slow runtime complexity

Uniform Cost Search: This is a complete search algorithm, if there is an optimal solution it will find it, if the step cost is >= c. Its time complexity is unique, as it is O(b^C*/d+1) where C* is the cost of the optimal solution we are looking for. Its space complexity follows suite with O(b^C*/d+1). It is however, optimal

**Based on my understanding, which search strategy is best for three different situations:**

Equal action cost and dense solution space: I believe DFS would be the best search method to use in this situation, as DFS can be much faster than BFS (and UCS, since it is equal action cost) in finding solutions in dense solution spaces. Along with its linear space complexity, it is the best in this scenario

Equal action cost and d is significantly smaller than m: I believe that BFS is the best approach to this scenario. It is optimal in equal action cost environments, and its runtime would not be slower than DFS, since DFS' runtime becomes bloated when m is much larger than d. UCS can be considered, but since it is equal action cost it will act like BFS.  

Actions with different costs: UCS is the best search method in this scenario. With different costs for actions, UCS is able to search through each node with the lowest costs, continuing to search all possible paths with increasing costs. It is complete in this scenario and it will find the optimal solution. 