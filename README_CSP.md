## Jacob Lebold
Bachelor of Science in Computer Science

## Q/A
**How long did it take me to complete this assignment?**

2 hours

**What is the most challenging part for you?**

The most challenging part was the last problem. Following the arc consistency was a challenge for me. Although I don't believe I was able to acheive the correct answer, my understanding of arc consistency was better than when I started. 

**Compare traditional search and CSPs then indicate the suitable situations for CSPs**

Traditional Search requires that the order in which we set the variables of the problem matter, as the solution is an ordered sequence of actions. While in CSPs, the order does not matter, as the solution is a set of assignments that can be in any order as we see fit. CSP's are useful in situations where we have a set of variables with known domains and constraints that are involved in a situation. 

**Give a few real-world problems that can be solved by CSPs, why would it be a better option than traditional search?**

A couple few world problems are:
    Assignment problems (who works at what station)
    Timetabling problems (which class is offered when and where)
    Transportation scheduling. 
CSPs would be better options for these problems as we have defined constraints. For example, deciding who works at what station we have the constraint of what workers are trained for particular positions, and how many hours they can work in a single week. For Timetabling problems, the constraints are the time in which classes can be offered, as they cannot be at night, and cannot overlap with other classes (they cannot be taught in the same classroom at the same time). 